#!/bin/env bash
set -e

#./podman_login.sh

cd ..

podman build -t financialplanner -f Dockerfile

podman tag localhost/financialplanner quay.io/breniserbrian/financialplanner:latest

podman push quay.io/breniserbrian/financialplanner:latest

notify-send "Done building and uploading podman container to quay"

