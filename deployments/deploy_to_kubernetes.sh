#!/bin/env bash
set -e

./podman_login.sh

podman build -t financialplanner -f Dockerfile

podman tag localhost/financialplanner quay.io/breniserbrian/financialplanner:latest

podman push quay.io/breniserbrian/financialplanner:latest

kubectl -n financialplanner rollout restart deployment/frontend

notify-send "Done uploading Financial Planner to Kubernetes"

