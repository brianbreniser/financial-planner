FROM rust:latest as build

# Build our working directory
RUN USER=root cargo new --bin /rust-app
WORKDIR /rust-app

# Rust dependencies
RUN rustup target add wasm32-unknown-unknown \
    && useradd -Um appuser

RUN cargo install --locked trunk

COPY Cargo.toml     Cargo.toml
COPY Cargo.lock     Cargo.lock

RUN cargo build --release \
    && cargo clean --release

COPY Trunk.toml     Trunk.toml
COPY README.md      README.md
COPY index.html     index.html
COPY styles         styles
COPY src            src

#RUN trunk build --release
RUN trunk build --release

RUN chown -R appuser:appuser /rust-app

USER appuser
WORKDIR /rust-app

EXPOSE 8080

CMD ["trunk", "serve", "--release"]
