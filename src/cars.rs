use yew::{html, virtual_dom::AttrValue, Component, Context, Html, Properties};

pub enum Msg {
    Calculate,
}

#[allow(dead_code)]
pub struct Cars {
    num_years_as_driver: f64,
    num_years_each_car_owned: f64,
    num_cars_in_life: f64,
    miles_each_car_driven: f64,
    insurance_per_6_month: f64,
    miles_per_kwh: f64,
    price_per_kwh: f64,
    miles_per_gallon: f64,
    price_per_gallon: f64,
    car_average_price: f64,
    car_average_sell: f64,
    inereste_rate: f64,
    display_result: f64,
}

fn default_header() -> AttrValue {
    AttrValue::from("Calculate cost of a car")
}

#[derive(PartialEq, Properties)]
pub struct Props {
    #[prop_or_else(default_header)]
    header_text: AttrValue,
    // #[prop_or(false)]
    // show_help_message: bool,
}

impl Component for Cars {
    type Message = Msg;
    type Properties = Props;

    fn create(_ctx: &Context<Self>) -> Self {
        Cars {
            num_years_as_driver: 0.0,
            num_years_each_car_owned: 0.0,
            num_cars_in_life: 0.0,
            miles_each_car_driven: 0.0,
            insurance_per_6_month: 0.0,
            miles_per_kwh: 0.0,
            price_per_kwh: 0.0,
            miles_per_gallon: 0.0,
            price_per_gallon: 0.0,
            car_average_price: 0.0,
            car_average_sell: 0.0,
            inereste_rate: 0.0,
            display_result: 0.0,
        }
    }

    // the return type of this determines if you need to re-render
    fn update(&mut self, _ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Msg::Calculate => {
                self.display_result += 1.0;
                true
            }
        }
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let link = ctx.link(); // Allows us to set the scope
        let header_text = &ctx.props().header_text;

        let calculate = link.callback(|_| Msg::Calculate);

        html! {
            <div>
                <p class={"center"}>{ header_text }</p>
                <div class="center-center">
                    <button onclick={ calculate }>{ "Calculate the cost of a car" }</button>
                </div>
            </div>
        }
    }
}
