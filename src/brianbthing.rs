use yew::{Component, html};

pub enum Msg {
    Add,
    Minus
}

pub struct OtherCounter {
    value: usize,
}

impl Component for OtherCounter {
    type Message = Msg;
    type Properties = ();

    fn create(_ctx: &yew::Context<Self>) -> Self {
        Self {
            value: 0
        }
    }

    fn update(&mut self, _ctx: &yew::Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Msg::Add => {
                self.value += 1;
                true
            }

            Msg::Minus => {
                self.value -= 1;
                true
            }
        }
    }

    fn view(&self, ctx: &yew::Context<Self>) -> yew::Html {
        html!(
            <div>
                <button onclick={ ctx.link().callback(|_| Msg::Add) } >{ "+1" }</button>
                <p>{ self.value }</p>
                <button onclick={ ctx.link().callback(|_| Msg::Minus) } >{ "-1" }</button>
            </div>
        )
    }

}
