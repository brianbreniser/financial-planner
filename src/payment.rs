use crate::{fmt_string::fmt_string_as_currency, logic::loan::calculate_payment};
use gloo::storage::{LocalStorage, Storage};
use rust_decimal::Decimal;
use rust_decimal_macros::dec;
use serde_derive::{Deserialize, Serialize};
use web_sys::HtmlInputElement;
use yew::{html, virtual_dom::AttrValue, Component, Context, Event, Html, Properties, TargetCast};

use crate::DEBUG;

extern crate log;

// ==================================== Msg ==

pub enum Msg {
    Calculate,
    Reset,
    UpdatePrincipal(String),
    UpdateInterest(String),
    UpdateYears(String),
}

// ==================================== Struct ==
#[allow(unused)]
#[derive(Debug, Serialize, Deserialize)]
pub struct Payoff {
    principal: Decimal,
    interest: Decimal,
    years: Decimal,
    calculated_monthly_payment: Decimal,
}

// ==================================== Props ==
#[derive(Clone, PartialEq, Properties)]
pub struct Props {
    #[prop_or(default_key())]
    key: &'static str,
}

fn default_key() -> &'static str {
    "payment_key"
}

// ==================================== Component ==
impl Component for Payoff {
    type Message = Msg;
    type Properties = Props;

    fn create(ctx: &Context<Self>) -> Self {
        let stored_data = LocalStorage::get(&ctx.props().key).unwrap_or_else(|_| Payoff {
            principal: dec!(0),
            interest: dec!(0),
            years: dec!(0),
            calculated_monthly_payment: dec!(0),
        });

        Payoff {
            principal: stored_data.principal,
            interest: stored_data.interest,
            years: stored_data.years,
            calculated_monthly_payment: stored_data.calculated_monthly_payment,
        }
    }

    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Msg::Calculate => {
                self.calculate();
            }
            Msg::Reset => {
                self.reset();
            }
            Msg::UpdatePrincipal(input) => {
                let input = input.parse::<Decimal>().unwrap_or(dec!(0));
                self.principal = input;
                self.calculate();
            }
            Msg::UpdateInterest(input) => {
                let input = input.parse::<Decimal>().unwrap_or(dec!(0));
                self.interest = input;
                self.calculate();
            }
            Msg::UpdateYears(input) => {
                let input = input.parse::<Decimal>().unwrap_or(dec!(0));
                self.years = input;
                self.calculate();
            }
        }

        LocalStorage::set(&ctx.props().key, &self).expect("Failed to store data in LocalStorage");
        true
    }

    fn rendered(&mut self, _ctx: &Context<Self>, _first_render: bool) {
        log::info!("We've rendered!");
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let link = ctx.link(); // Allows us to set the scope

        let calculate = link.callback(|_| Msg::Calculate);
        let reset = link.callback(|_| Msg::Reset);

        let stored_data = LocalStorage::get(&ctx.props().key).unwrap_or_else(|_| Payoff {
            principal: dec!(0),
            interest: dec!(0),
            years: dec!(0),
            calculated_monthly_payment: dec!(0),
        });

        html! {
            <>
            <div>
                <p class={ "center" }>{ "Payoff a debt" }</p>
                { self.principal_html(ctx, &stored_data) }
                { self.interest_html(ctx, &stored_data) }
                { self.years_html(ctx, &stored_data) }
                { self.results_html() }
                <div class="center-center">
                    <button class="action-button" onclick={ calculate }>{ "Re-Calculate" }</button>
                    <button class="action-button grey" onclick={ reset }>{ "reset" }</button>
                </div>
            </div>
            </>
        }
    }
}

// ==================================== Html Helpers ==

impl Payoff {
    fn results_html(&self) -> Html {
        html! {
            <>
            <div class="result-section">
                if DEBUG {
                    <p class="center">{ "DEBUG: Raw results" }</p>
                    <div class="center-center">
                        { format!("DEBUG: Raw Decimal: {}", self.calculated_monthly_payment) }
                    </div>
                }
                <div class="result-item">
                    <span class="center">{ "Payment/mo" }</span>
                    <div class="center">
                        { fmt_string_as_currency(self.calculated_monthly_payment.to_string()) }
                    </div>
                </div>
                <div class="result-item">
                    <span class="center">{ "Interest" }</span>
                    <div class="center red">
                        { fmt_string_as_currency(((self.calculated_monthly_payment * self.years * dec!(12)) - self.principal).to_string()) }
                    </div>
                </div>
                <div class="result-item">
                    <span class="center">{ "Total Paid" }</span>
                    <div class="center">
                        { fmt_string_as_currency((self.calculated_monthly_payment * self.years * dec!(12)).to_string()) }
                    </div>
                </div>
            </div>
            </>
        }
    }

    fn principal_html(&self, ctx: &Context<Self>, stored_data: &Payoff) -> Html {
        let update_principal = ctx.link().batch_callback(|event: Event| {
            let input: HtmlInputElement = event.target_unchecked_into();
            let value = input.value();
            Some(Msg::UpdatePrincipal(value))
        });

        html! {
            <>
            <div class={ "input_block" }>
                <label class={ "input_label" }>
                    { "Principal" }
                </label>
                <input class={ "input_input" }
                    placeholder={ AttrValue::from(stored_data.principal.to_string())}
                    onchange={ update_principal }
                />
            </div>
            </>
        }
    }

    fn interest_html(&self, ctx: &Context<Self>, stored_data: &Payoff) -> Html {
        let update_interest = ctx.link().batch_callback(|event: Event| {
            let input: HtmlInputElement = event.target_unchecked_into();
            let value = input.value();
            Some(Msg::UpdateInterest(value))
        });

        html! {
            <>
            <div class={ "input_block" }>
                <label class={ "input_label" }>
                    { "Interest" }
                </label>
                <input class={ "input_input" }
                    placeholder={ AttrValue::from(stored_data.interest.to_string())}
                    onchange={ update_interest }
                />
            </div>
            </>
        }
    }

    fn years_html(&self, ctx: &Context<Self>, stored_data: &Payoff) -> Html {
        let update_years = ctx.link().batch_callback(|event: Event| {
            let input: HtmlInputElement = event.target_unchecked_into();
            let value = input.value();
            Some(Msg::UpdateYears(value))
        });

        html! {
            <>
            <div class={ "input_block" }>
                <label class={ "input_label" }>
                    { "Years" }
                </label>
                <input class={ "input_input" }
                    placeholder={ AttrValue::from(stored_data.years.to_string())}
                    onchange={ update_years }
                />
            </div>
            </>
        }
    }
}

// ==================================== Business logic ==

impl Payoff {
    fn calculate(&mut self) {
        self.calculated_monthly_payment = self.calculate_monthly_payment();
    }

    fn reset(&mut self) {
        self.calculated_monthly_payment = dec!(0);
        self.principal = dec!(0);
        self.interest = dec!(0);
        self.years = dec!(0);
    }

    #[allow(unused)]
    fn calculate_monthly_payment(&self) -> Decimal {
        return calculate_payment(self.principal, self.interest, self.years);
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_0() {
        let p = Payoff {
            principal: dec!(0),
            interest: dec!(0),
            years: dec!(0),
            calculated_monthly_payment: dec!(0),
        };

        assert_eq!(dec!(0), p.calculate_monthly_payment().round_dp(2));
    }

    #[test]
    fn test_no_interest() {
        let p = Payoff {
            principal: dec!(12000),
            interest: dec!(0),
            years: dec!(1),
            calculated_monthly_payment: dec!(0),
        };

        assert_eq!(dec!(1000), p.calculate_monthly_payment().round_dp(2));
    }

    #[test]
    fn test_only_principal() {
        let p = Payoff {
            principal: dec!(1000),
            interest: dec!(0),
            years: dec!(0),
            calculated_monthly_payment: dec!(0),
        };

        assert_eq!(dec!(1000), p.calculate_monthly_payment().round_dp(2));
    }

    #[test]
    fn test_no_principal() {
        let p = Payoff {
            principal: dec!(0),
            interest: dec!(5),
            years: dec!(30),
            calculated_monthly_payment: dec!(0),
        };

        assert_eq!(dec!(0), p.calculate_monthly_payment().round_dp(2));
    }

    #[test]
    fn test_no_years() {
        let p = Payoff {
            principal: dec!(100000),
            interest: dec!(5),
            years: dec!(0),
            calculated_monthly_payment: dec!(0),
        };

        assert_eq!(dec!(100000), p.calculate_monthly_payment().round_dp(2));
    }

    #[test]
    fn test_boring_example() {
        let p = Payoff {
            principal: dec!(100000),
            interest: dec!(5),
            years: dec!(30),
            calculated_monthly_payment: dec!(0),
        };

        assert_eq!(dec!(536.82), p.calculate_monthly_payment().round_dp(2));
    }

    #[test]
    fn test_realistic_example() {
        let p = Payoff {
            principal: dec!(530000),
            interest: dec!(2.75),
            years: dec!(30),
            calculated_monthly_payment: dec!(0),
        };

        assert_eq!(dec!(2163.68), p.calculate_monthly_payment().round_dp(2));
    }
}
