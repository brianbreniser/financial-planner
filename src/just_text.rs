use yew::{function_component, Html, html};

#[function_component(JustText)]
pub fn just_text() -> Html {
    html! {
        <div>
            <p>{ "This is an empty component" }</p>
        </div>
    }
}
