use rust_decimal::{Decimal, MathematicalOps};
use rust_decimal_macros::dec;

// TODO: Refactor compounded, contributed to be here
use crate::compounding_interest::{Compounded, Contributed};

#[allow(dead_code)]
pub fn calculate_compounding_interest_only_principal(
    principal: Decimal,
    interest: Decimal,
    inflation: Decimal,
    time_in_years: Decimal,
    compounded: Compounded,
) -> Decimal {
    if interest == dec!(0) && inflation == dec!(0) {
        return principal;
    }

    let interest_decimal = (interest - inflation) / dec!(100);

    let right_base = dec!(1) + interest_decimal / compounded.value_decimal();
    let right_exponent = compounded.value_decimal() * time_in_years;
    let right = right_base.powd(right_exponent);

    return principal * right;
}

#[allow(dead_code)]
pub fn calculate_compounding_interest_of_contributions(
    contribution: Decimal,
    interest: Decimal,
    inflation: Decimal,
    years: Decimal,
    compounded: Compounded,
    contributed: Contributed,
) -> Decimal {
    if interest == dec!(0) && inflation == dec!(0) {
        return contribution * years * contributed.value_decimal();
    }

    let interest_decimal = (interest - inflation) / dec!(100);

    let left_base = dec!(1) + (interest_decimal / compounded.value_decimal());
    let left_exponent = compounded.value_decimal() * years;

    let left: Decimal = left_base.powd(left_exponent) - dec!(1);
    let right: Decimal = interest_decimal / compounded.value_decimal();

    let result =
        contribution * (contributed.value_decimal() / compounded.value_decimal()) * left / right;
    return result;
}

#[allow(dead_code)]
pub fn calculate_compounding_interest(
    principal: Decimal,
    payment: Decimal,
    interest: Decimal,
    inflation: Decimal,
    years: Decimal,
    compounded: Compounded,
    contributed: Contributed,
) -> Decimal {
    return (calculate_compounding_interest_only_principal(
        principal, interest, inflation, years, compounded,
    ) + calculate_compounding_interest_of_contributions(
        payment,
        interest,
        inflation,
        years,
        compounded,
        contributed,
    ))
    .round_dp(2);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn does_decimal_parse_with_all_kinds_of_input() {
        assert_eq!(dec!(0), "0".parse::<Decimal>().unwrap());
        assert_eq!(dec!(0), "0.0".parse::<Decimal>().unwrap());
        assert_eq!(dec!(18), "18".parse::<Decimal>().unwrap());
        assert_eq!(dec!(18), "18.0".parse::<Decimal>().unwrap());
        assert_eq!(dec!(0.0), "0".parse::<Decimal>().unwrap());
        assert_eq!(dec!(1.1), "1.1".parse::<Decimal>().unwrap());
        assert_eq!(dec!(100000), "100000".parse::<Decimal>().unwrap());
        assert_eq!(
            dec!(100000.12),
            "100000.1234".parse::<Decimal>().unwrap().round_dp(2)
        );
    }

    #[test]
    fn principal_0() {
        let principal: Decimal = dec!(0);
        let interest: Decimal = dec!(0);
        let inflation: Decimal = dec!(0);
        let time_in_years: Decimal = dec!(0);
        let compounded: Compounded = Compounded::Monthly;

        assert_eq!(
            calculate_compounding_interest_only_principal(
                principal,
                interest,
                inflation,
                time_in_years,
                compounded
            )
            .round_dp(2),
            dec!(0.00)
        )
    }

    #[test]
    fn principal_1000() {
        let principal: Decimal = dec!(1000);
        let interest: Decimal = dec!(0);
        let inflation: Decimal = dec!(0);
        let time_in_years: Decimal = dec!(0);
        let compounded: Compounded = Compounded::Monthly;

        assert_eq!(
            calculate_compounding_interest_only_principal(
                principal,
                interest,
                inflation,
                time_in_years,
                compounded
            )
            .round_dp(2),
            dec!(1000.00)
        )
    }

    #[test]
    fn principal_10000() {
        let principal: Decimal = dec!(10000);
        let interest: Decimal = dec!(5);
        let inflation: Decimal = dec!(0);
        let time_in_years: Decimal = dec!(30);
        let compounded: Compounded = Compounded::Monthly;

        assert_eq!(
            calculate_compounding_interest_only_principal(
                principal,
                interest,
                inflation,
                time_in_years,
                compounded
            )
            .round_dp(2),
            dec!(44677.44)
        )
    }

    #[test]
    fn contribution_0() {
        let contribution: Decimal = dec!(0);
        let interest: Decimal = dec!(5);
        let inflation: Decimal = dec!(0);
        let years: Decimal = dec!(30);
        let compounded: Compounded = Compounded::Monthly;
        let contributed: Contributed = Contributed::Monthly;

        assert_eq!(
            calculate_compounding_interest_of_contributions(
                contribution,
                interest,
                inflation,
                years,
                compounded,
                contributed,
            )
            .round_dp(2),
            dec!(0.00)
        )
    }

    #[test]
    fn contribution_1000() {
        let contribution: Decimal = dec!(1000);
        let interest: Decimal = dec!(0);
        let inflation: Decimal = dec!(0);
        let years: Decimal = dec!(1);
        let compounded: Compounded = Compounded::Monthly;
        let contributed: Contributed = Contributed::Monthly;

        assert_eq!(
            calculate_compounding_interest_of_contributions(
                contribution,
                interest,
                inflation,
                years,
                compounded,
                contributed,
            )
            .round_dp(2),
            dec!(12000.00)
        )
    }

    #[test]
    fn contribution_1000_with_interest() {
        let contribution: Decimal = dec!(1000);
        let interest: Decimal = dec!(5);
        let inflation: Decimal = dec!(0);
        let years: Decimal = dec!(30);
        let compounded: Compounded = Compounded::Monthly;
        let contributed: Contributed = Contributed::Monthly;

        assert_eq!(
            calculate_compounding_interest_of_contributions(
                contribution,
                interest,
                inflation,
                years,
                compounded,
                contributed,
            )
            .round_dp(2),
            dec!(832258.64)
        )
    }

    #[test]
    fn contribution_1000_test_contribution() {
        let contribution: Decimal = dec!(1000);
        let interest: Decimal = dec!(5);
        let inflation: Decimal = dec!(0);
        let years: Decimal = dec!(30);
        let compounded: Compounded = Compounded::Monthly;
        let contributed: Contributed = Contributed::Annually;

        assert_eq!(
            calculate_compounding_interest_of_contributions(
                contribution,
                interest,
                inflation,
                years,
                compounded,
                contributed,
            )
            .round_dp(2),
            dec!(69354.89)
        )
    }

    #[test]
    fn total_0() {
        let principal = dec!(0);
        let payment = dec!(0);
        let interest = dec!(0);
        let inflation = dec!(0);
        let years = dec!(0);
        let compounded = Compounded::Monthly;
        let contributed = Contributed::Monthly;

        assert_eq!(
            calculate_compounding_interest(
                principal,
                payment,
                interest,
                inflation,
                years,
                compounded,
                contributed,
            ),
            dec!(0)
        )
    }

    #[test]
    fn total_100000() {
        let principal = dec!(100000);
        let payment = dec!(1000);
        let interest = dec!(8);
        let inflation = dec!(0);
        let years = dec!(30);
        let compounded = Compounded::Monthly;
        let contributed = Contributed::Monthly;

        assert_eq!(
            calculate_compounding_interest(
                principal,
                payment,
                interest,
                inflation,
                years,
                compounded,
                contributed,
            ),
            dec!(2583932.41)
        )
    }

    #[test]
    fn total_1000_with_inflation() {
        let principal = dec!(100000);
        let payment = dec!(1000);
        let years = dec!(30);
        let compounded = Compounded::Monthly;
        let contributed = Contributed::Monthly;

        assert_eq!(
            calculate_compounding_interest(
                principal,
                payment,
                dec!(8), // 8 interest
                dec!(3), // with 3 inflation
                years,
                compounded,
                contributed,
            ),
            // is the same as
            calculate_compounding_interest(
                principal,
                payment,
                dec!(5), // 5 interest
                dec!(0), // but no inflation
                years,
                compounded,
                contributed,
            )
        )
    }
}
