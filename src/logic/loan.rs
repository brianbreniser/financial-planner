use rust_decimal::{Decimal, MathematicalOps};
use rust_decimal_macros::dec;

#[allow(dead_code)]
pub fn calculate_payment(principal: Decimal, interest: Decimal, years: Decimal) -> Decimal {
    let p = principal;
    let i = interest / dec!(100) / dec!(12);
    let n = years * dec!(12);

    // Early return, fix div/0 problems
    if no_principal(p) {
        return dec!(0);
    }

    if no_interest(i) {
        if no_months(n) {
            return p;
        }

        return p / n;
    }

    if no_months(n) {
        return p;
    }

    let left = i * (dec!(1) + i).powd(n);
    let right = (dec!(1) + i).powd(n) - dec!(1);

    let m = p * (left / right);

    m
}

// ==================================== Helpers ==

#[allow(dead_code)]
fn no_interest(i: Decimal) -> bool {
    i == dec!(0)
}

#[allow(dead_code)]
fn no_principal(i: Decimal) -> bool {
    i == dec!(0)
}

#[allow(dead_code)]
fn no_months(i: Decimal) -> bool {
    i == dec!(0)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn calculate_payment_0() {
        let payment = dec!(0);
        let interest = dec!(0);
        let years = dec!(0);

        assert_eq!(
            dec!(0),
            calculate_payment(payment, interest, years).round_dp(2)
        );
    }

    #[test]
    fn calculate_payment_1200() {
        let payment = dec!(1200);
        let interest = dec!(0);
        let years = dec!(0);

        assert_eq!(
            dec!(1200),
            calculate_payment(payment, interest, years).round_dp(2)
        );
    }

    #[test]
    fn calculate_payment_1200_months() {
        let payment = dec!(1200);
        let interest = dec!(0);
        let years = dec!(1);

        assert_eq!(
            dec!(100),
            calculate_payment(payment, interest, years).round_dp(2)
        );
    }

    #[test]
    fn calculate_payment_realistic_car() {
        let payment = dec!(24597);
        let interest = dec!(6.5);
        let years = dec!(6);

        assert_eq!(
            dec!(413.47),
            calculate_payment(payment, interest, years).round_dp(2)
        );
    }
}
