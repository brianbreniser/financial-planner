use crate::dynamic_calculator::dynamic_calculator_state::State;
use yew::{function_component, html, Html};
// use web_sys::HtmlInputElement;
use super::compounding_interest_view::compounding_interest_view;
use super::display_a_field_view::display_a_field_view;
use super::payoff_debt_view::payoff_debt_view;
use crate::DEBUG;
use yewdux::prelude::use_store;

#[function_component(DynamicAppController)]
pub fn dynamic_calculator_controller() -> Html {
    let (state, dispatch) = use_store::<State>();

    let reset_state = dispatch.reduce_mut_callback(|state| {
        state.reset_all_lists();
    });

    let mut add_new_compunding_interest_item =
        dispatch.reduce_mut_callback(|state| state.create_new_compounding_interest());
    let mut add_new_payoff_debt_item =
        dispatch.reduce_mut_callback(|state| state.create_new_payoff_debt());
    let mut add_new_display_item =
        dispatch.reduce_mut_callback(|state| state.create_new_display_a_field());

    if DEBUG {
        add_new_compunding_interest_item = dispatch
            .reduce_mut_callback(|state| state.create_new_compounding_interest_with_random_data());
        add_new_payoff_debt_item =
            dispatch.reduce_mut_callback(|state| state.create_new_payoff_debt_with_random_data());
        add_new_display_item =
            dispatch.reduce_mut_callback(|state| state.create_new_compounding_interest());
    }

    let compounding_interest_items =
        (0..state.compounding_interest_list.len().clone()).collect::<Vec<_>>();
    let payoff_debt_items = (0..state.payoff_debt_list.len().clone()).collect::<Vec<_>>();
    let display_items = (0..state.display_fields.len().clone()).collect::<Vec<_>>();

    html! {
        <>
            <div class="item item-flex-start item-max-width-xsm">
                <p class={ "center bold" }>{"Control Center"}</p>
                <p>{"New items will be added to the end of the current list of items."}</p>
                <button onclick={add_new_compunding_interest_item}> {"NEW Compunding Interest"} </button>
                <br />
                <button onclick={add_new_payoff_debt_item}> {"NEW Debt Payoff"} </button>
                <br />
                <button onclick={add_new_display_item}> {"NEW Result Tracker"} </button>
                <br />
                <button onclick={reset_state}> {"REMOVE all apps"} </button>
                if DEBUG {
                    <p> {format!("Compounding interest list has <{}> items", state.compounding_interest_list.len())} </p>
                }
            </div>
            <>
                {
                   compounding_interest_items
                        .iter()
                        .enumerate()
                        .map(|i|
                            compounding_interest_view(i.0, state.clone(), dispatch.clone())
                        ).collect::<Html>()
                }
            </>
            <>
                {
                    payoff_debt_items
                        .iter()
                        .enumerate()
                        .map(|i|
                            payoff_debt_view(i.0, state.clone(), dispatch.clone())
                        ).collect::<Html>()
                }
            </>
            <>
                {
                    display_items
                        .iter()
                        .enumerate()
                        .map(|i|
                            display_a_field_view(i.0, state.clone(), dispatch.clone())
                        ).collect::<Html>()
                }

            </>
        </>
    }
}

/* Keeping to remind me how to use Properties on a function component
#[derive(PartialEq, Properties)]
struct CivProps {
    position: usize,
}

#[function_component(CompoundingInterestView)]
fn compounding_interest_view(props: &CivProps) -> Html {
    let (state, _) = use_store::<State>();

    let my_data = state.format_compounding_interest_item_at_position(props.position);

    html! {
        <div class="item">
            <p>{"This is a section"}</p>
            <p>{"Our data: "}</p>
            <p>{format!("Compounding interest item: {}", my_data)}</p>
        </div>
    }
}
*/
