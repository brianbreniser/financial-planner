// use serde::Serialize;
// use serde_derive::Deserialize;

#[cfg(test)]
mod tests {
    use rust_decimal::Decimal;
    use rust_decimal_macros::dec;
    // use super::*;

    #[test]
    fn does_decimal_parse_with_all_kinds_of_input() {
        assert_eq!(dec!(0), "0".parse::<Decimal>().unwrap());
        assert_eq!(dec!(0), "0.0".parse::<Decimal>().unwrap());
        assert_eq!(dec!(18), "18".parse::<Decimal>().unwrap());
        assert_eq!(dec!(18), "18.0".parse::<Decimal>().unwrap());
        assert_eq!(dec!(0.0), "0".parse::<Decimal>().unwrap());
        assert_eq!(dec!(1.1), "1.1".parse::<Decimal>().unwrap());
        assert_eq!(dec!(100000), "100000".parse::<Decimal>().unwrap());
        assert_eq!(
            dec!(100000.12),
            "100000.1234".parse::<Decimal>().unwrap().round_dp(2)
        );
    }

}
