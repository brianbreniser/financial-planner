use rust_decimal::{Decimal, MathematicalOps};
use rust_decimal_macros::dec;

#[allow(dead_code)]
pub fn calculate_years(principal: Decimal, interest: Decimal, payment: Decimal) -> Decimal {
    let p = principal;
    let i = interest / dec!(100) / dec!(12);
    let m = payment;

    // Early return, fix div/0 problems
    if no_principal(p) {
        return dec!(0);
    }

    if no_interest(i) {
        if no_payment(m) {
            return dec!(0);
        }

        return p / m;
    }

    if no_payment(m) {
        return dec!(0);
    }

    let left = (dec!(1) - (p * i) / m).ln();
    let right = (dec!(1) + i).ln();

    let months = -(left / right);
    let years = months / dec!(12);

    years.round_dp(2)
}

#[allow(dead_code)]
pub fn calculate_payment(principal: Decimal, interest: Decimal, years: Decimal) -> Decimal {
    let p = principal;
    let i = interest / dec!(100) / dec!(12);
    let n = years * dec!(12);

    // Early return, fix div/0 problems
    if no_principal(p) {
        return dec!(0);
    }

    if no_interest(i) {
        if no_months(n) {
            return p;
        }

        return p / n;
    }

    if no_months(n) {
        return p;
    }

    let left = i * (dec!(1) + i).powd(n);
    let right = (dec!(1) + i).powd(n) - dec!(1);

    let m = p * (left / right);

    m
}

#[allow(dead_code)]
fn no_interest(i: Decimal) -> bool {
    i == dec!(0)
}

#[allow(dead_code)]
fn no_payment(i: Decimal) -> bool {
    i == dec!(0)
}

#[allow(dead_code)]
fn no_principal(i: Decimal) -> bool {
    i == dec!(0)
}

#[allow(dead_code)]
fn no_months(i: Decimal) -> bool {
    i == dec!(0)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn calculate_payment_0() {
        let payment = dec!(0);
        let interest = dec!(0);
        let years = dec!(0);

        assert_eq!(
            dec!(0),
            calculate_payment(payment, interest, years).round_dp(2)
        );
    }

    #[test]
    fn calculate_payment_1200() {
        let payment = dec!(1200);
        let interest = dec!(0);
        let years = dec!(0);

        assert_eq!(
            dec!(1200),
            calculate_payment(payment, interest, years).round_dp(2)
        );
    }

    #[test]
    fn calculate_payment_1200_months() {
        let payment = dec!(1200);
        let interest = dec!(0);
        let years = dec!(1);

        assert_eq!(
            dec!(100),
            calculate_payment(payment, interest, years).round_dp(2)
        );
    }

    #[test]
    fn calculate_payment_realistic_car() {
        let payment = dec!(24597);
        let interest = dec!(6.5);
        let years = dec!(6);

        assert_eq!(
            dec!(413.47),
            calculate_payment(payment, interest, years).round_dp(2)
        );
    }

    #[test]
    fn calculate_years_0() {
        let principal = dec!(0);
        let interest = dec!(0);
        let payment = dec!(0);

        assert_eq!(
            dec!(0),
            calculate_years(principal, interest, payment).round_dp(2)
        );
    }

    #[test]
    fn calculate_years_1200() {
        let principal = dec!(1200);
        let interest = dec!(0);
        let payment = dec!(0);

        assert_eq!(
            dec!(0),
            calculate_years(principal, interest, payment).round_dp(2)
        );
    }

    #[test]
    fn calculate_years_realistic() {
        let principal = dec!(24597);
        let interest = dec!(6.5);
        let payment = dec!(413.47);

        assert_eq!(
            dec!(6),
            calculate_years(principal, interest, payment).round_dp(2)
        );
    }

    #[test]
    fn calculate_years_realistic_2() {
        let principal = dec!(57397.23);
        let interest = dec!(4.239);
        let payment = dec!(976.49);

        assert_eq!(
            dec!(5.5),
            calculate_years(principal, interest, payment).round_dp(2)
        );
    }
}
