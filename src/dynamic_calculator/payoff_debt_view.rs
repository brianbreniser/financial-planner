use std::rc::Rc;
use rust_decimal::{Decimal, prelude::FromPrimitive};
use web_sys::HtmlInputElement;
use yew::{html, Html, Event, TargetCast};
use yewdux::prelude::Dispatch;
use crate::{fmt_string::fmt_string_as_currency, dynamic_calculator::dynamic_calculator_state::PayoffDebtCalculationType};

use super::dynamic_calculator_state::State;

pub fn payoff_debt_view(i: usize, state: Rc<State>, dispatch: Dispatch<State>) -> Html {
    if state.payoff_debt_list.len() == 0 {
        return html!{<></>}
    }

    let delete_me = dispatch.reduce_mut_callback(move |state| state.delete_payoff_debt_at(i));

    let calculate = dispatch.reduce_mut_callback_with(move |state, _| {
        state.payoff_debt_list[i].calculate();
    });

    let reset = dispatch.reduce_mut_callback_with(move |state, _| {
        state.payoff_debt_list[i].reset();
    });

    // Calculate early, this will be the first time we calculate if the item was just created
    dispatch.reduce_mut(move |state| {
        state.payoff_debt_list[i].calculate();
    });

    let id = state.payoff_debt_list[i].id.to_string();

    html!{
        <div class={ "item item-max-width-sm" }>
            <p class={ "center bold no-bottom" }>{ "Debt Payoff" }</p>
            <p class={ "center no-top small-text" }>{ id }</p>
            { principal_html(i, state.clone(), dispatch.clone()) }
            { interest_html(i, state.clone(), dispatch.clone()) }
            { payment_type_html(i, state.clone(), dispatch.clone()) }

            if state.payoff_debt_list[i].calculation_type == PayoffDebtCalculationType::Payment {
                { payment_html(i, state.clone(), dispatch.clone()) }
            } else {
                { years_html(i, state.clone(), dispatch.clone()) }
            }

            { results_html(i, state.clone(), dispatch.clone()) }
            <div class="center">
                <button class="action-button grey" onclick={ reset }>{ "Reset" }</button>
                <button class="action-button" onclick={ calculate }>{ "Calculate" }</button>
                <button class="action-button grey" onclick={delete_me}> {"REMOVE"} </button>
            </div>
        </div>
    }
}

pub fn principal_html(i: usize, state: Rc<State>, dispatch: Dispatch<State>) -> Html {
    let label = "Principal:";
    let id = "principal";
    let value = state.payoff_debt_list[i].principal.to_string();

    let update_value = dispatch.reduce_mut_callback_with(move |state, event: Event| {
        let input: HtmlInputElement = event.target_unchecked_into();

        if let Ok(val) = input.value().parse::<f32>() {
            state.payoff_debt_list[i].principal = Decimal::from_f32(val).unwrap_or_default();
            state.payoff_debt_list[i].calculate();
        }
    });

    return html! {
            <div class={ "input_block" }>
                <label class={ "input_label grid-center " }>
                    { label }
                </label>
                <input 
                    class={ "input_input" }
                    value={ value }
                    onchange={ update_value }
                    id={ id }
                />
            </div>
    }
}

pub fn interest_html(i: usize, state: Rc<State>, dispatch: Dispatch<State>) -> Html {
    let label = "Interest:";
    let id = "interest";
    let value = state.payoff_debt_list[i].interest.to_string();

    let update_value = dispatch.reduce_mut_callback_with(move |state, event: Event| {
        let input: HtmlInputElement = event.target_unchecked_into();

        if let Ok(val) = input.value().parse::<f32>() {
            state.payoff_debt_list[i].interest = Decimal::from_f32(val).unwrap_or_default();
            state.payoff_debt_list[i].calculate();
        }
    });

    return html! {
            <div class={ "input_block" }>
                <label class={ "input_label grid-center " }>
                    { label }
                </label>
                <input 
                    class={ "input_input" }
                    value={ value }
                    onchange={ update_value }
                    id={ id }
                />
            </div>
    }
}

pub fn payment_type_html(i: usize, state: Rc<State>, dispatch: Dispatch<State>) -> Html {
    let set_payment = dispatch.reduce_mut_callback_with(move |state, _| {
        state.payoff_debt_list[i].calculation_type = PayoffDebtCalculationType::Payment;
        state.payoff_debt_list[i].calculate();
    });

    let set_years = dispatch.reduce_mut_callback_with(move |state, _| {
        state.payoff_debt_list[i].calculation_type = PayoffDebtCalculationType::Years;
        state.payoff_debt_list[i].calculate();
    });

    return html! {
        <>
        <div class={ "input_block" }>
            <div><label class={ "input_label grid-center" }>{ "Calculation Type:" }</label></div>
            <div class={ "row" }>
                <button class={ if state.payoff_debt_list[i].calculation_type == PayoffDebtCalculationType::Payment {"item-chosen"} else {"item-not-chosen"} } onclick={ set_payment }>
                    { "Payment" }
                </button>
                <button class={ if state.payoff_debt_list[i].calculation_type == PayoffDebtCalculationType::Years {"item-chosen"} else {"item-not-chosen"} } onclick={ set_years }>
                    { "Years" }
                </button>
            </div>
        </div>
        </>
    }
}

pub fn years_html(i: usize, state: Rc<State>, dispatch: Dispatch<State>) -> Html {
    let label = "Years";
    let id = "years";
    let value = state.payoff_debt_list[i].years.to_string();

    let update_value = dispatch.reduce_mut_callback_with(move |state, event: Event| {
        let input: HtmlInputElement = event.target_unchecked_into();

        if let Ok(val) = input.value().parse::<f32>() {
            state.payoff_debt_list[i].years = Decimal::from_f32(val).unwrap_or_default();
            state.payoff_debt_list[i].calculate();
        }
    });

    return html! {
            <div class={ "input_block" }>
                <label class={ "input_label grid-center " }>
                    { label }
                </label>
                <input 
                    class={ "input_input" }
                    value={ value }
                    onchange={ update_value }
                    id={ id }
                />
            </div>
    }

}

pub fn payment_html(i: usize, state: Rc<State>, dispatch: Dispatch<State>) -> Html {
    let label = "Payment:";
    let id = "payment";
    let value = state.payoff_debt_list[i].payment.to_string();

    let update_value = dispatch.reduce_mut_callback_with(move |state, event: Event| {
        let input: HtmlInputElement = event.target_unchecked_into();

        if let Ok(val) = input.value().parse::<f32>() {
            state.payoff_debt_list[i].payment = Decimal::from_f32(val).unwrap_or_default();
            state.payoff_debt_list[i].calculate();
        }
    });

    return html! {
            <div class={ "input_block" }>
                <label class={ "input_label grid-center " }>
                    { label }
                </label>
                <input 
                    class={ "input_input" }
                    value={ value }
                    onchange={ update_value }
                    id={ id }
                />
            </div>
    }
}

pub fn results_html(i: usize, state: Rc<State>, _dispatch: Dispatch<State>) -> Html {
    let monthly_payment = state.payoff_debt_list[i].result_monthly_payment;
    let years = state.payoff_debt_list[i].result_years_to_payoff;
    let interest = state.payoff_debt_list[i].result_interest;
    let total_paid = state.payoff_debt_list[i].result_total_paid;

    return html! {
        <>
            <div class="result-section">
                <div class="result-item">
                    <span class="center">{ "Payment/mo" }</span>
                    <div class="center">
                        { fmt_string_as_currency(monthly_payment.to_string())}
                    </div>
                </div>
                <div class="result-item">
                    <span class="center">{ "Years" }</span>
                    <div class="center">
                        { years.to_string()}
                    </div>
                </div>
                <div class="result-item">
                    <span class="center">{ "Interest" }</span>
                    <div class="center green">
                        { fmt_string_as_currency(interest.to_string()) }
                    </div>
                </div>
                <div class="result-item">
                    <span class="center">{ "Total Paid" }</span>
                    <div class="center-center">
                        { fmt_string_as_currency(total_paid.to_string()) }
                    </div>
                </div>
            </div>
        </>
    }

}
