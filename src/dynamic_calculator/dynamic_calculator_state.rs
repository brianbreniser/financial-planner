#![allow(dead_code)] // TODO: Remove this when done

use std::{
    fmt::{Display, Formatter},
    str::FromStr,
};

use rand;
use rust_decimal::{prelude::FromPrimitive, Decimal};
use rust_decimal_macros::dec;
use serde::{Deserialize, Serialize};
use yewdux::prelude::Store;

use super::payoff_debt_logic::{calculate_payment, calculate_years};
use crate::logic::compounding_interest_logic_v2::calculate_compounding_interest;

// ###############################################################
// Extend the decimal system to support generating random integers
// ###############################################################

pub trait HasRandom<T> {
    fn random(min: u64, max: u64) -> T;
}

impl HasRandom<Decimal> for Decimal {
    fn random(min: u64, max: u64) -> Decimal {
        if min == max {
            // Can't % with 0, min == max, so range = 0
            // This isn't a random number, but when min == max that is exptected
            return Decimal::from_u64(min).unwrap_or(Decimal::default());
        }

        let range = max - min;
        let random_int: u64 = rand::random::<u64>() % range + min;

        Decimal::from_u64(random_int).unwrap_or(Decimal::default())
    }
}

#[inline(always)]
pub fn random_usize() -> usize {
    return rand::random::<usize>();
}

// #####
// State
// #####

#[derive(Debug, Default, Clone, PartialEq, Eq, Deserialize, Serialize, Store)]
#[store(storage = "local")]
pub struct State {
    // TODO: An accumulator item, one that adds up all the CI or PD items
    pub compounding_interest_list: Vec<CompoundingInterestSection>,
    pub payoff_debt_list: Vec<PayoffDebtSection>,
    pub display_fields: Vec<DisplayAField>,
}

impl State {
    pub fn get_compounding_interest_as_stringified_list(&self) -> String {
        format!("{:?}", self.compounding_interest_list)
    }

    pub fn get_payoff_debt_as_stringified_list(&self) -> String {
        format!("{:?}", self.payoff_debt_list)
    }

    pub fn reset_all_lists(&mut self) {
        self.compounding_interest_list = vec![];
        self.payoff_debt_list = vec![];
        self.display_fields = vec![];
    }

    // ##############################
    // Compounding Interest section
    // ##############################
    pub fn get_calculate_interest_at_position_as_string(&self, position: usize) -> String {
        let result = self.compounding_interest_list.get(position);
        format!("{:?}", result)
    }

    pub fn create_new_compounding_interest(&mut self) {
        self.compounding_interest_list
            .push(CompoundingInterestSection::new());
    }

    pub fn create_new_compounding_interest_with_random_data(&mut self) {
        self.compounding_interest_list
            .push(CompoundingInterestSection::new_with_random_data());
    }

    pub fn delete_compounding_interest_at(&mut self, i: usize) {
        let _ = self.compounding_interest_list.remove(i);
    }

    // ##############################
    // Payoff Debt section
    // ##############################

    pub fn get_payoff_debt_at_position_as_string(&self, position: usize) -> String {
        let result = self.payoff_debt_list.get(position);
        format!("{:?}", result)
    }

    pub fn create_new_payoff_debt(&mut self) {
        self.payoff_debt_list.push(PayoffDebtSection::new());
    }

    pub fn create_new_payoff_debt_with_random_data(&mut self) {
        self.payoff_debt_list
            .push(PayoffDebtSection::new_with_random_data());
    }

    pub fn delete_payoff_debt_at(&mut self, i: usize) {
        let _ = self.payoff_debt_list.remove(i);
    }

    // ##############################
    // Display A Field section
    // ##############################

    pub fn get_display_a_field_at_position_as_string(&self, position: usize) -> String {
        let result = self.display_fields.get(position);
        format!("{:?}", result)
    }

    pub fn create_new_display_a_field(&mut self) {
        let id = self.compounding_interest_list[0].id;
        self.display_fields.push(DisplayAField::new(id));
    }

    pub fn delete_display_a_field_at(&mut self, i: usize) {
        let _ = self.display_fields.remove(i);
    }
}

// ##############################
// Payoff Debt section
// ##############################

#[derive(Debug, Clone, PartialEq, Eq, Deserialize, Serialize, Store)]
pub enum PayoffDebtCalculationType {
    Payment,
    Years,
}

impl Default for PayoffDebtCalculationType {
    fn default() -> Self {
        PayoffDebtCalculationType::Payment
    }
}

#[derive(Debug, Default, Clone, PartialEq, Eq, Deserialize, Serialize, Store)]
pub struct PayoffDebtSection {
    pub calculation_type: PayoffDebtCalculationType,
    pub id: usize,

    pub principal: Decimal,
    pub interest: Decimal,
    pub years: Decimal,
    pub payment: Decimal,

    pub result_monthly_payment: Decimal,
    pub result_interest: Decimal,
    pub result_total_paid: Decimal,
    pub result_years_to_payoff: Decimal,
}

impl PayoffDebtSection {
    pub fn new() -> PayoffDebtSection {
        PayoffDebtSection {
            calculation_type: PayoffDebtCalculationType::default(),
            id: random_usize(),
            principal: Decimal::default(),
            interest: Decimal::default(),
            years: Decimal::default(),
            payment: Decimal::default(),
            result_monthly_payment: Decimal::default(),
            result_interest: Decimal::default(),
            result_total_paid: Decimal::default(),
            result_years_to_payoff: Decimal::default(),
        }
    }

    pub fn new_with_random_data() -> PayoffDebtSection {
        PayoffDebtSection {
            calculation_type: PayoffDebtCalculationType::default(),
            id: random_usize(),
            principal: Decimal::random(500, 20000),
            interest: Decimal::random(2, 15),
            years: Decimal::random(1, 10),
            payment: Decimal::default(),
            result_monthly_payment: Decimal::default(),
            result_interest: Decimal::default(),
            result_total_paid: Decimal::default(),
            result_years_to_payoff: Decimal::default(),
        }
    }

    pub fn get_item_value(&self, item: PayoffDebtField) -> Decimal {
        match item {
            PayoffDebtField::ResultMonthlyPayment => self.result_monthly_payment,
            PayoffDebtField::ResultInterest => self.result_interest,
            PayoffDebtField::ResultTotalPaid => self.result_total_paid,
        }
    }

    pub fn calculate(&mut self) {
        let months_in_year = dec!(12);
        self.result_monthly_payment = Decimal::default();
        self.result_interest = Decimal::default();
        self.result_total_paid = Decimal::default();
        self.result_years_to_payoff = Decimal::default();

        if self.calculation_type == PayoffDebtCalculationType::Payment {
            self.result_years_to_payoff =
                calculate_years(self.principal, self.interest, self.payment);
            self.years = self.result_years_to_payoff;
            self.result_monthly_payment = self.payment;
        } else {
            self.result_monthly_payment =
                calculate_payment(self.principal, self.interest, self.years);
            self.payment = self.result_monthly_payment;
            self.result_years_to_payoff = self.years;
        }
        self.result_total_paid = self.result_monthly_payment * months_in_year * self.years;
        self.result_interest = self.result_total_paid - self.principal;
    }

    pub fn reset(&mut self) {
        self.principal = Decimal::default();
        self.interest = Decimal::default();
        self.years = Decimal::default();
        self.payment = Decimal::default();
        self.result_monthly_payment = Decimal::default();
        self.result_interest = Decimal::default();
        self.result_total_paid = Decimal::default();
        self.result_years_to_payoff = Decimal::default();
    }
}

// ##############################
// Compounding Interest section
// ##############################

#[derive(Debug, Default, Clone, PartialEq, Eq, Deserialize, Serialize, Store)]
pub struct CompoundingInterestSection {
    pub id: usize,
    pub principal: Decimal,
    pub contributions: Decimal,
    pub interest: Decimal,
    pub inflation: Decimal,
    pub years: Decimal,
    pub final_result: Decimal,
    pub total_contributions: Decimal,
    pub total_interest: Decimal,
    pub compounded: Compounded,
    pub contributed: Contributed,
}

impl CompoundingInterestSection {
    pub fn new() -> CompoundingInterestSection {
        CompoundingInterestSection {
            id: random_usize(),
            principal: Decimal::default(),
            contributions: Decimal::default(),
            interest: Decimal::default(),
            inflation: Decimal::default(),
            years: Decimal::default(),
            final_result: Decimal::default(),
            total_contributions: Decimal::default(),
            total_interest: Decimal::default(),
            compounded: Compounded::default(),
            contributed: Contributed::default(),
        }
    }

    pub fn new_with_random_data() -> CompoundingInterestSection {
        CompoundingInterestSection {
            id: random_usize(),
            principal: Decimal::random(10000, 1000000),
            contributions: Decimal::random(250, 5000),
            interest: Decimal::random(2, 12),
            inflation: Decimal::random(0, 4),
            years: Decimal::random(1, 40),
            final_result: Decimal::default(),
            total_contributions: Decimal::default(),
            total_interest: Decimal::default(),
            compounded: Compounded::default(),
            contributed: Contributed::default(),
        }
    }

    pub fn get_item_value(&self, item: CompoundingInterestField) -> Decimal {
        match item {
            CompoundingInterestField::FinalResult => self.final_result,
            CompoundingInterestField::TotalContributions => self.total_contributions,
            CompoundingInterestField::TotalInterest => self.total_interest,
        }
    }

    pub fn calculate(&mut self) {
        self.final_result = calculate_compounding_interest(
            self.principal,
            self.contributions,
            self.interest,
            self.inflation,
            self.years,
            self.compounded,
            self.contributed,
        );

        self.total_contributions =
            self.principal + (self.contributions * self.contributed.value_decimal() * self.years);

        self.total_interest = self.final_result - self.total_contributions;
    }

    pub fn reset(&mut self) {
        self.principal = Decimal::default();
        self.contributions = Decimal::default();
        self.interest = Decimal::default();
        self.inflation = Decimal::default();
        self.years = Decimal::default();
        self.compounded = Compounded::default();
        self.contributed = Contributed::default();
        self.final_result = Decimal::default();
    }
}

#[derive(Debug, Default, Clone, Copy, PartialEq, Eq, Deserialize, Serialize, Store)]
pub enum Compounded {
    Annually,
    #[default]
    Monthly,
    Daily,
}

impl Compounded {
    pub fn value_decimal(&self) -> Decimal {
        match *self {
            Compounded::Annually => dec!(1),
            Compounded::Monthly => dec!(12),
            Compounded::Daily => dec!(360),
        }
    }
}

#[derive(Debug, Default, Clone, Copy, PartialEq, Eq, Deserialize, Serialize, Store)]
pub enum Contributed {
    Annually,
    Quarterly,
    #[default]
    Monthly,
    Biweekly,
}

impl Contributed {
    pub fn value_decimal(&self) -> Decimal {
        match *self {
            Contributed::Annually => dec!(1),
            Contributed::Quarterly => dec!(4),
            Contributed::Monthly => dec!(12),
            Contributed::Biweekly => dec!(26),
        }
    }
}

// ##############################
// Display a field section
// ##############################

#[derive(Debug, Default, Clone, Copy, PartialEq, Eq, Deserialize, Serialize, Store)]
pub enum ListType {
    #[default]
    CompoundingInterest,
    PayoffDebt,
}

impl Display for ListType {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        match *self {
            ListType::CompoundingInterest => write!(f, "CI"),
            ListType::PayoffDebt => write!(f, "PD"),
        }
    }
}

// derives copy trait
#[derive(Debug, Default, Clone, Copy, PartialEq, Eq, Deserialize, Serialize, Store)]
pub enum CompoundingInterestField {
    #[default]
    FinalResult,
    TotalContributions,
    TotalInterest,
}

impl Display for CompoundingInterestField {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        match *self {
            CompoundingInterestField::FinalResult => write!(f, "Final Value"),
            CompoundingInterestField::TotalContributions => write!(f, "Contributions"),
            CompoundingInterestField::TotalInterest => write!(f, "Interest"),
        }
    }
}

impl FromStr for CompoundingInterestField {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "Final Result" => Ok(CompoundingInterestField::FinalResult),
            "Total Contributions" => Ok(CompoundingInterestField::TotalContributions),
            "Total Interest" => Ok(CompoundingInterestField::TotalInterest),
            _ => Err(()),
        }
    }
}

#[derive(Debug, Default, Clone, Copy, PartialEq, Eq, Deserialize, Serialize, Store)]
pub enum PayoffDebtField {
    #[default]
    ResultMonthlyPayment,
    ResultInterest,
    ResultTotalPaid,
}

impl Display for PayoffDebtField {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        match *self {
            PayoffDebtField::ResultMonthlyPayment => write!(f, "Monthly Payment"),
            PayoffDebtField::ResultInterest => write!(f, "Interest Paid"),
            PayoffDebtField::ResultTotalPaid => write!(f, "Total Paid"),
        }
    }
}

impl FromStr for PayoffDebtField {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "Result Monthly Payment" => Ok(PayoffDebtField::ResultMonthlyPayment),
            "Result Interest" => Ok(PayoffDebtField::ResultInterest),
            "Result Total Paid" => Ok(PayoffDebtField::ResultTotalPaid),
            _ => Err(()),
        }
    }
}

#[derive(Debug, Default, Clone, Copy, PartialEq, Eq, Deserialize, Serialize, Store)]
pub struct DisplayAField {
    pub id: usize,
    pub reference_id: usize,
    pub list_type: ListType,
    pub ci_field: CompoundingInterestField,
    pub pd_field: PayoffDebtField,
    pub calculated_value: Option<Decimal>,
}

impl DisplayAField {
    pub fn new(id: usize) -> DisplayAField {
        DisplayAField {
            id: random_usize(),
            reference_id: id,
            list_type: ListType::default(),
            ci_field: CompoundingInterestField::default(),
            pd_field: PayoffDebtField::default(),
            calculated_value: None,
        }
    }

    pub fn get_ci_value(&self, ci_list: &Vec<CompoundingInterestSection>) -> Result<Decimal, ()> {
        // find where the self.reference_id matches the id of the ci_list and call get_item_value
        for ci in ci_list {
            if ci.id == self.reference_id {
                return Result::Ok(ci.get_item_value(self.ci_field));
            }
        }

        return Result::Err(());
    }

    pub fn get_pd_value(&self, pd_list: &Vec<PayoffDebtSection>) -> Result<Decimal, ()> {
        // find where the self.reference_id matches the id of the pd_list and call get_item_value
        for pd in pd_list {
            if pd.id == self.reference_id {
                return Result::Ok(pd.get_item_value(self.pd_field));
            }
        }

        return Result::Err(());
    }

    pub fn calculate_monthly(&mut self, value: Decimal) {
        let monthly = value / dec!(12);
        self.calculated_value = Some(monthly);
    }

    pub fn display_as_string(&self) -> String {
        format!("{:?}", self)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn random_decimal_works_with_min_equal_max() {
        assert_eq!(dec!(1), Decimal::random(1, 1));
    }

    // If these occationally fail, do some digging
    #[test]
    fn random_decimal_max_works_with_small_ranges() {
        assert!(Decimal::random(1, 10) <= dec!(10));
        assert!(Decimal::random(1, 10) <= dec!(10));
        assert!(Decimal::random(1, 10) <= dec!(10));
        assert!(Decimal::random(1, 10) <= dec!(10));
        assert!(Decimal::random(1, 10) <= dec!(10));
    }

    // If these occationally fail, do some digging
    #[test]
    fn random_decimal_min_works_with_small_ranges() {
        assert!(Decimal::random(1, 10) >= dec!(1));
        assert!(Decimal::random(1, 10) >= dec!(1));
        assert!(Decimal::random(1, 10) >= dec!(1));
        assert!(Decimal::random(1, 10) >= dec!(1));
        assert!(Decimal::random(1, 10) >= dec!(1));
    }

    #[test]
    fn random_decimal_max_works_with_max_u64() {
        let max = Decimal::from_u64(u64::MAX).unwrap_or(Decimal::default());

        assert!(Decimal::random(u64::MAX - 10, u64::MAX) <= max);
        assert!(Decimal::random(u64::MAX - 10, u64::MAX) <= max);
        assert!(Decimal::random(u64::MAX - 10, u64::MAX) <= max);
        assert!(Decimal::random(u64::MAX - 10, u64::MAX) <= max);
        assert!(Decimal::random(u64::MAX - 10, u64::MAX) <= max);
    }

    #[test]
    fn random_decimal_min_works_with_max_u64() {
        let max_minus_10 = Decimal::from_u64(u64::MAX - 10).unwrap_or(Decimal::default());

        assert!(Decimal::random(u64::MAX - 10, u64::MAX) >= max_minus_10);
        assert!(Decimal::random(u64::MAX - 10, u64::MAX) >= max_minus_10);
        assert!(Decimal::random(u64::MAX - 10, u64::MAX) >= max_minus_10);
        assert!(Decimal::random(u64::MAX - 10, u64::MAX) >= max_minus_10);
        assert!(Decimal::random(u64::MAX - 10, u64::MAX) >= max_minus_10);
    }
}
