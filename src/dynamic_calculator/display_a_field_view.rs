use crate::{
    dynamic_calculator::dynamic_calculator_state::{CompoundingInterestField, PayoffDebtField},
    fmt_string::fmt_string_as_currency,
};
use rust_decimal_macros::dec;
use std::rc::Rc;
use web_sys::HtmlInputElement;
use yew::{html, Event, Html, TargetCast};
use yewdux::prelude::Dispatch;

use super::dynamic_calculator_state::State;

use crate::dynamic_calculator::dynamic_calculator_state::ListType;

pub fn display_a_field_view(i: usize, state: Rc<State>, dispatch: Dispatch<State>) -> Html {
    let current_list = state.display_fields[i].list_type;

    let delete_me = dispatch.reduce_mut_callback(move |state| state.delete_display_a_field_at(i));

    html! {
        <>
            <div class="item">
                <p class={ "center bold" }>{ "Result Tracker" }</p>
                { type_html(i, state.clone(), dispatch.clone()) }
                { reference_html(i, state.clone(), dispatch.clone()) }
                { if current_list == ListType::CompoundingInterest { ci_field_html(i, state.clone(), dispatch.clone()) }
                    else { pd_field_html(i, state.clone(), dispatch.clone()) } }

                <div class="center">
                    <button class="action-button grey" onclick={delete_me}> {"REMOVE"} </button>
                </div>
            </div>
        </>
    }
}

pub fn type_html(i: usize, state: Rc<State>, dispatch: Dispatch<State>) -> Html {
    let set_ci = dispatch.reduce_mut_callback_with(move |state, _| {
        state.display_fields[i].list_type = ListType::CompoundingInterest;
    });

    let set_pd = dispatch.reduce_mut_callback_with(move |state, _| {
        state.display_fields[i].list_type = ListType::PayoffDebt;
    });

    let current_option = state.display_fields[i].list_type;

    html! {
        <>
        <div class={ "input_block" }>
            <div><label class={ "input_label grid-center" }>{ "Contributed:" }</label></div>
            <div class={ "row" }>
                <button class={ if current_option == ListType::CompoundingInterest {"item-chosen"} else {"item-not-chosen"} } onclick={ set_ci }>
                    { "Compound" }
                </button>
                <button class={ if current_option == ListType::PayoffDebt {"item-chosen"} else {"item-not-chosen"} } onclick={ set_pd }>
                    { "Payoff" }
                </button>
            </div>
        </div>
        </>
    }
}

pub fn reference_html(i: usize, state: Rc<State>, dispatch: Dispatch<State>) -> Html {
    let label = "Reference";
    let id = "reference";
    let value = state.display_fields[i].reference_id.to_string();

    let update_value = dispatch.reduce_mut_callback_with(move |state, event: Event| {
        let input: HtmlInputElement = event.target_unchecked_into();

        if let Ok(val) = input.value().parse::<usize>() {
            state.display_fields[i].reference_id = val;
        }
    });

    return html! {
        <div class={ "input_block" }>
            <label class={ "input_label grid-center " }>
                { label }
            </label>
            <input
                class={ "input_input" }
                value={ value }
                onchange={ update_value }
                id={ id }
            />
        </div>
    };
}

pub fn ci_field_html(i: usize, state: Rc<State>, dispatch: Dispatch<State>) -> Html {
    let result = state.display_fields[i]
        .get_ci_value(&state.compounding_interest_list)
        .unwrap_or_default();

    let calculate_monthly = dispatch.reduce_mut_callback_with(move |state, _| {
        state.display_fields[i].calculated_value = Some(result / dec!(12));
    });

    let set_ci_field = dispatch.reduce_mut_callback_with(move |state, event: Event| {
        let input: HtmlInputElement = event.target_unchecked_into();

        if let Ok(val) = input.value().parse::<CompoundingInterestField>() {
            state.display_fields[i].ci_field = val;
            state.display_fields[i].calculated_value = None;
        }
    });

    html! {
        <>
        <div class={ "input_block" }>
            <label class={ "input_label grid-center " }>
                { "Field:" }
            </label>
            <select onchange={ set_ci_field }>
                <option value="Final Result">{ "Final Value" }</option>
                <option value="Total Contributions">{ "Contributions" }</option>
                <option value="Total Interest">{ "Interest" }</option>
            </select>
        </div>
        <div class="center">
            <button class="action-button" onclick={calculate_monthly}>{ "Calculate Monthly" }</button>
        </div>
        <div class="result-section">
            <div class="result-item">
                <span class="center">{ format!("{}", state.display_fields[i].ci_field) }</span>
                <div class="center-center">
                    { fmt_string_as_currency(format!("{}", result)) }
                </div>
            </div>
            if let Some(calculated) = state.display_fields[i].calculated_value {
                <div class="result-item">
                    <span class="center">{ "Monthly Amount" }</span>
                    <div class="center-center">
                        { fmt_string_as_currency(format!("{}", calculated)) }
                    </div>
                </div>
            }
        </div>
        </>
    }
}

pub fn pd_field_html(i: usize, state: Rc<State>, dispatch: Dispatch<State>) -> Html {
    let result = state.display_fields[i]
        .get_pd_value(&state.payoff_debt_list)
        .unwrap_or_default();

    let calculate_monthly = dispatch.reduce_mut_callback_with(move |state, _| {
        state.display_fields[i].calculated_value = Some(result / dec!(12));
    });

    let set_pd_field = dispatch.reduce_mut_callback_with(move |state, event: Event| {
        let input: HtmlInputElement = event.target_unchecked_into();

        if let Ok(val) = input.value().parse::<PayoffDebtField>() {
            state.display_fields[i].pd_field = val;
            state.display_fields[i].calculated_value = None;
        }
    });

    html! {
        <>
        <div class={ "input_block" }>
            <label class={ "input_label grid-center " }>
                { "Field:" }
            </label>
            <select onchange={ set_pd_field }>
                <option value="Result Monthly payment ">{ "Monthly Payment" }</option>
                <option value="Result Interest">{ "Interest" }</option>
                <option value="Result Total Paid">{ "Total Paid" }</option>
            </select>
        </div>
        <div class="center">
            <button class="action-button" onclick={calculate_monthly}>{ "Calculate Monthly" }</button>
        </div>
        <div class="result-section">
            <div class="result-item">
                <span class="center">{ format!("{}", state.display_fields[i].pd_field) }</span>
                <div class="center-center">
                    { fmt_string_as_currency(format!("{}", result)) }
                </div>
            </div>
            if let Some(calculated) = state.display_fields[i].calculated_value {
                <div class="result-item">
                    <span class="center">{ "Monthly Amount" }</span>
                    <div class="center-center">
                        { fmt_string_as_currency(format!("{}", calculated)) }
                    </div>
                </div>
            }
        </div>
        </>
    }
}
