use super::dynamic_calculator_state::Compounded;
use crate::{
    dynamic_calculator::dynamic_calculator_state::{Contributed, State},
    fmt_string::fmt_string_as_currency,
};
use rust_decimal::{prelude::FromPrimitive, Decimal};
use std::rc::Rc;
use web_sys::HtmlInputElement;
use yew::{html, Event, Html, TargetCast};
use yewdux::prelude::Dispatch;

pub fn compounding_interest_view(i: usize, state: Rc<State>, dispatch: Dispatch<State>) -> Html {
    // Return early on empty compounding interest list
    if state.compounding_interest_list.len() == 0 {
        return html! {<></>};
    }

    let delete_me =
        dispatch.reduce_mut_callback(move |state| state.delete_compounding_interest_at(i));

    let calculate = dispatch.reduce_mut_callback_with(move |state, _| {
        state.compounding_interest_list[i].calculate();
    });

    let reset = dispatch.reduce_mut_callback_with(move |state, _| {
        state.compounding_interest_list[i].reset();
    });

    // Calculate early, this will be the first time we calculate if the item was just created
    dispatch.reduce_mut(move |state| {
        state.compounding_interest_list[i].calculate();
    });

    let id = state.compounding_interest_list[i].id.to_string();

    return html! {
        <div class={ "item item-max-width-sm" }>
            <p class={ "center bold no-bottom" }>{ "Compounding Interest" }</p>
            <p class={ "center no-top small-text" }>{ id }</p>
            { principal_html(i, state.clone(), dispatch.clone()) }
            { contributions_html(i, state.clone(), dispatch.clone()) }
            { contributed_html(i, state.clone(), dispatch.clone()) }
            { interest_html(i, state.clone(), dispatch.clone()) }
            { compounded_html(i, state.clone(), dispatch.clone()) }
            { inflation_html(i, state.clone(), dispatch.clone()) }
            { years_html(i, state.clone(), dispatch.clone()) }
            { results_html(i, state.clone()) }
            <div class="center">
                <button class="action-button grey" onclick={ reset }>{ "Reset" }</button>
                <button class="action-button" onclick={ calculate }>{ "Calculate" }</button>
                <button class="action-button grey" onclick={delete_me}> {"REMOVE"} </button>
            </div>
        </div>
    };
}

pub fn principal_html(i: usize, state: Rc<State>, dispatch: Dispatch<State>) -> Html {
    let label = "Principal:";
    let id = "principal";
    let value = state.compounding_interest_list[i].principal.to_string();

    let update_value = dispatch.reduce_mut_callback_with(move |state, event: Event| {
        let input: HtmlInputElement = event.target_unchecked_into();

        if let Ok(val) = input.value().parse::<f32>() {
            state.compounding_interest_list[i].principal =
                Decimal::from_f32(val).unwrap_or_default();
            state.compounding_interest_list[i].calculate();
        }
    });

    return html! {
            <div class={ "input_block" }>
                <label class={ "input_label grid-center " }>
                    { label }
                </label>
                <input
                    class={ "input_input" }
                    value={ value }
                    onchange={ update_value }
                    id={ id }
                />
            </div>
    };
}

pub fn contributions_html(i: usize, state: Rc<State>, dispatch: Dispatch<State>) -> Html {
    let label = "Contributions: ";
    let id = "contributions";
    let value = state.compounding_interest_list[i].contributions.to_string();

    let update_value = dispatch.reduce_mut_callback_with(move |state, event: Event| {
        let input: HtmlInputElement = event.target_unchecked_into();

        if let Ok(val) = input.value().parse::<f32>() {
            state.compounding_interest_list[i].contributions =
                Decimal::from_f32(val).unwrap_or_default();
            state.compounding_interest_list[i].calculate();
        }
    });

    return html! {
            <div class={ "input_block" }>
                <label class={ "input_label grid-center " }>
                    { label }
                </label>
                <input
                    class={ "input_input" }
                    value={ value }
                    onchange={ update_value }
                    id={ id }
                />
            </div>
    };
}

pub fn contributed_html(i: usize, state: Rc<State>, dispatch: Dispatch<State>) -> Html {
    let set_annually = dispatch.reduce_mut_callback_with(move |state, _| {
        state.compounding_interest_list[i].contributed = Contributed::Annually;
    });

    let set_quarterly = dispatch.reduce_mut_callback_with(move |state, _| {
        state.compounding_interest_list[i].contributed = Contributed::Quarterly;
    });

    let set_monthly = dispatch.reduce_mut_callback_with(move |state, _| {
        state.compounding_interest_list[i].contributed = Contributed::Monthly;
    });

    let set_biweekly = dispatch.reduce_mut_callback_with(move |state, _| {
        state.compounding_interest_list[i].contributed = Contributed::Biweekly;
    });

    let current_option = state.compounding_interest_list[i].contributed.clone();

    html! {
        <>
        <div class={ "input_block" }>
            <div><label class={ "input_label grid-center" }>{ "Contributed:" }</label></div>
            <div class={ "row" }>
                <button class={ if current_option == Contributed::Annually {"item-chosen"} else {"item-not-chosen"} } onclick={ set_annually }>
                    { "Annually" }
                </button>
                <button class={ if current_option == Contributed::Quarterly {"item-chosen"} else {"item-not-chosen"} } onclick={ set_quarterly }>
                    { "Quarterly" }
                </button>
                <button class={ if current_option == Contributed::Monthly {"item-chosen"} else {"item-not-chosen"} } onclick={ set_monthly }>
                    { "Monthly" }
                </button>
                <button class={ if current_option == Contributed::Biweekly {"item-chosen"} else {"item-not-chosen"} } onclick={ set_biweekly }>
                    { "bi-weekly" }
                </button>
            </div>
        </div>
        </>
    }
}

pub fn interest_html(i: usize, state: Rc<State>, dispatch: Dispatch<State>) -> Html {
    let label = "Interest:";
    let id = "interest";
    let value = state.compounding_interest_list[i].interest.to_string();

    let update_value = dispatch.reduce_mut_callback_with(move |state, event: Event| {
        let input: HtmlInputElement = event.target_unchecked_into();

        if let Ok(val) = input.value().parse::<f32>() {
            state.compounding_interest_list[i].interest =
                Decimal::from_f32(val).unwrap_or_default();
            state.compounding_interest_list[i].calculate();
        }
    });

    return html! {
        <div class={ "input_block" }>
            <label class={ "input_label grid-center " }>
                { label }
            </label>
            <input
                class={ "input_input" }
                value={ value }
                onchange={ update_value }
                id={ id }
            />
        </div>
    };
}

pub fn compounded_html(i: usize, state: Rc<State>, dispatch: Dispatch<State>) -> Html {
    let set_annually = dispatch.reduce_mut_callback_with(move |state, _| {
        state.compounding_interest_list[i].compounded = Compounded::Annually;
    });

    let set_monthly = dispatch.reduce_mut_callback_with(move |state, _| {
        state.compounding_interest_list[i].compounded = Compounded::Monthly;
    });

    let set_daily = dispatch.reduce_mut_callback_with(move |state, _| {
        state.compounding_interest_list[i].compounded = Compounded::Daily;
    });

    let current_option = state.compounding_interest_list[i].compounded.clone();

    html! {
        <>
        <div class={ "input_block" }>
            <div><label class={ "input_label grid-center" }>{ "Compounded:" }</label></div>
            <div class={ "row" }>
                <button class={ if current_option == Compounded::Annually {"item-chosen"} else {"item-not-chosen"} } onclick={ set_annually }>
                    { "Annually" }
                </button>
                <button class={ if current_option == Compounded::Monthly {"item-chosen"} else {"item-not-chosen"} } onclick={ set_monthly }>
                    { "Monthly" }
                </button>
                <button class={ if current_option == Compounded::Daily {"item-chosen"} else {"item-not-chosen"} } onclick={ set_daily }>
                    { "Daily" }
                </button>
            </div>
        </div>
        </>
    }
}

pub fn inflation_html(i: usize, state: Rc<State>, dispatch: Dispatch<State>) -> Html {
    let label = "Inflation:";
    let id = "inflation";
    let value = state.compounding_interest_list[i].inflation.to_string();

    let update_value = dispatch.reduce_mut_callback_with(move |state, event: Event| {
        let input: HtmlInputElement = event.target_unchecked_into();

        if let Ok(val) = input.value().parse::<f32>() {
            state.compounding_interest_list[i].inflation =
                Decimal::from_f32(val).unwrap_or_default();
            state.compounding_interest_list[i].calculate();
        }
    });

    return html! {
            <div class={ "input_block" }>
                <label class={ "input_label grid-center " }>
                    { label }
                </label>
                <input
                    class={ "input_input" }
                    value={ value }
                    onchange={ update_value }
                    id={ id }
                />
            </div>
    };
}

pub fn years_html(i: usize, state: Rc<State>, dispatch: Dispatch<State>) -> Html {
    let label = "Years:";
    let id = "years";
    let value = state.compounding_interest_list[i].years.to_string();

    let update_value = dispatch.reduce_mut_callback_with(move |state, event: Event| {
        let input: HtmlInputElement = event.target_unchecked_into();

        if let Ok(val) = input.value().parse::<f32>() {
            state.compounding_interest_list[i].years = Decimal::from_f32(val).unwrap_or_default();
            state.compounding_interest_list[i].calculate();
        }
    });

    return html! {
            <div class={ "input_block" }>
                <label class={ "input_label grid-center " }>
                    { label }
                </label>
                <input
                    class={ "input_input" }
                    value={ value }
                    onchange={ update_value }
                    id={ id }
                />
            </div>
    };
}

pub fn results_html(i: usize, state: Rc<State>) -> Html {
    return html! {
        <>
            <div class="result-section">
                <div class="result-item">
                    <span class="center">{ "Contributions" }</span>
                    <div class="center">
                        { fmt_string_as_currency(state.compounding_interest_list[i].total_contributions.to_string()) }
                    </div>
                </div>
                <div class="result-item">
                    <span class="center">{ "Interest" }</span>
                    <div class="center green">
                        { fmt_string_as_currency(state.compounding_interest_list[i].total_interest.to_string()) }
                    </div>
                </div>
                <div class="result-item">
                    <span class="center">{ "Final Value" }</span>
                    <div class="center-center">
                        { fmt_string_as_currency(state.compounding_interest_list[i].final_result.to_string()) }
                    </div>
                </div>
            </div>
        </>
    };
}
