// Real stuff
mod base_module;
mod cars;
mod compounding_interest;
mod logic;
mod payment;

// New site
mod dynamic_calculator;

// Helpers
mod fmt_string;
mod wasm_helper;

// Testing these things
mod brianbthing;
mod just_text;
mod state;
mod test_app;

const DEBUG: bool = false;

fn main() {
    wasm_logger::init(wasm_logger::Config::default());
    yew::Renderer::<base_module::Base>::new().render();
}
