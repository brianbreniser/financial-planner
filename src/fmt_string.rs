#[allow(dead_code)]
pub fn fmt_string_as_currency(input: String) -> String {
    if input.len() == 0
        || input == "0".to_string()
        || input == "0.0".to_string()
        || input == "0.00".to_string()
    {
        return "$0.00".to_string();
    }
    if input.len() <= 3 {
        return format!("${}", input);
    }

    let mut r = input.clone();

    let mut cents: Option<&str> = None;
    if input.contains(".") {
        let mut contents: Vec<&str> = input.split(".").collect();
        cents = contents.pop();
        r = contents.pop().unwrap().to_string();
    }

    let mut index = 3;

    while index < r.len() {
        r.insert(r.len() - index, ',');

        index += 4;
    }

    match cents {
        Some(x) => {
            let slice = &x[..2];
            r.insert(r.len(), '.');
            r.insert_str(r.len(), slice);
        }
        None => {}
    }

    return format!("${}", r);
}

#[allow(dead_code)]
fn reverse_string(input: String) -> String {
    input.rsplit("").collect()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_empty_string() {
        assert_eq!("$0.00".to_string(), fmt_string_as_currency("".to_string()));
        assert_eq!("$0.00".to_string(), fmt_string_as_currency("0".to_string()));
        assert_eq!("$0.00".to_string(), fmt_string_as_currency("0.0".to_string()));
        assert_eq!("$0.00".to_string(), fmt_string_as_currency("0.00".to_string()));
    }

    #[test]
    fn test_int() {
        assert_eq!("$123".to_string(), fmt_string_as_currency("123".to_string()));
        assert_eq!("$1,123".to_string(), fmt_string_as_currency("1123".to_string()));
        assert_eq!("$987,123".to_string(), fmt_string_as_currency("987123".to_string()));
        assert_eq!("$5,987,123".to_string(), fmt_string_as_currency("5987123".to_string()));
        assert_eq!(
            "$125,987,123".to_string(),
            fmt_string_as_currency("125987123".to_string())
        );
        assert_eq!(
            "$8,125,987,123".to_string(),
            fmt_string_as_currency("8125987123".to_string())
        );
        assert_eq!(
            "$1,234,123,412,341,234,123,409,873,459,086,720,435".to_string(),
            fmt_string_as_currency("1234123412341234123409873459086720435".to_string())
        );
    }

    #[test]
    fn test_float() {
        assert_eq!("$100.00".to_string(), fmt_string_as_currency("100.00".to_string()));
        assert_eq!("$1,100.00".to_string(), fmt_string_as_currency("1100.00".to_string()));
        assert_eq!(
            "$987,100.00".to_string(),
            fmt_string_as_currency("987100.00".to_string())
        );
        assert_eq!(
            "$1,987,100.00".to_string(),
            fmt_string_as_currency("1987100.00".to_string())
        );
        assert_eq!(
            "$761,987,100.00".to_string(),
            fmt_string_as_currency("761987100.00".to_string())
        );
        assert_eq!(
            "$4,761,987,100.00".to_string(),
            fmt_string_as_currency("4761987100.00".to_string())
        );
        assert_eq!(
            "$1,234,123,412,341,234,123,409,873,459,086,720,435.45".to_string(),
            fmt_string_as_currency("1234123412341234123409873459086720435.45".to_string())
        );
    }

    #[test]
    fn test_large_float() {
        assert_eq!("$100.12".to_string(), fmt_string_as_currency("100.12345".to_string()));
    }
}
