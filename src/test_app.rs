use yew::{function_component, html, Component, Context, Html};
use yewdux::prelude::use_store;

use crate::state::test_counter::TestCounter;

pub enum Msg {
    Add(i64),
    Minus(i64),
}

pub struct Counter {
    value: i64,
}

impl Component for Counter {
    type Message = Msg;
    type Properties = ();

    fn create(_ctx: &Context<Self>) -> Self {
        Counter { value: 0 }
    }

    // the return type of this determines if you need to re-render
    fn update(&mut self, _ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Msg::Add(amount) => {
                self.value += amount;
                true
            }

            Msg::Minus(amount) => {
                self.value -= amount;
                true
            }
        }
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let link = ctx.link(); // Allows us to set the scope

        let add_one = link.callback(|_| Msg::Add(1));
        let minus_one = link.callback(|_| Msg::Minus(1));

        html! {
            <div>
                <Internal />
                <Internal2 />
                <p class={"center"}> { "Just testing" } </p>
                <div class="center-center">
                    <button onclick={ add_one }>{ "+1" }</button>
                </div>

                <p class={"center"}>{ self.value }</p>

                <div class="center-center">
                    <button onclick={ minus_one }>{ "-1" }</button>
                </div>
            </div>
        }
    }
}

#[function_component(Internal)]
fn internal() -> Html {
    let (state, dispatch) = use_store::<TestCounter>();
    let onclick = dispatch.reduce_mut_callback(|state| state.counter += 1);

    html! {
        <>
            <p>{ "Inside internal" }</p>
            <p>{ state.counter }</p>
            <button {onclick}>{"+1 again"}</button>
        </>
    }
}

#[function_component(Internal2)]
fn internal2() -> Html {
    let (state, dispatch) = use_store::<TestCounter>();
    let onclick = dispatch.reduce_mut_callback(|state| state.counter += 1);

    html! {
        <>
            <p>{ "Inside internal" }</p>
            <p>{ state.counter }</p>
            <button {onclick}>{"+1 again"}</button>
        </>
    }
}
