use wasm_bindgen::prelude::wasm_bindgen;

// ==================================== external ==
#[wasm_bindgen]
extern "C" {
    fn alert(s: &str);
}

#[wasm_bindgen]
#[allow(dead_code)]
pub fn greet(name: &str) {
    alert(&format!("Hello, {}!", name));
}

#[wasm_bindgen]
#[allow(dead_code)]
pub fn greet_string(name: String) {
    alert(&format!("Hello, {}!", name));
}
// ==================================== external ==
