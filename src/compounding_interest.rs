use gloo::storage::{LocalStorage, Storage};
use rust_decimal::Decimal;
use rust_decimal_macros::dec;
use serde::Serialize;
use serde_derive::Deserialize;
use std::fmt::Debug;
use web_sys::HtmlInputElement;
use yew::{html, virtual_dom::AttrValue, Component, Context, Event, Html, Properties, TargetCast};

use crate::logic::compounding_interest::calculate_compounding_interest;
use crate::{fmt_string::fmt_string_as_currency, DEBUG};

extern crate log;

// ==================================== Compound Enum ==
#[derive(Debug, Copy, Clone, PartialEq, Serialize, Deserialize)]
pub enum Compounded {
    Annually,
    Daily,
    Monthly,
}

impl Compounded {
    pub fn value_decimal(&self) -> Decimal {
        match *self {
            Compounded::Annually => dec!(1),
            Compounded::Daily => dec!(360),
            Compounded::Monthly => dec!(12),
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Serialize, Deserialize)]
pub enum Contributed {
    Annually,
    Quarterly,
    Monthly,
    Biweekly,
}

impl Contributed {
    pub fn value_decimal(&self) -> Decimal {
        match *self {
            Contributed::Annually => dec!(1),
            Contributed::Quarterly => dec!(4),
            Contributed::Monthly => dec!(12),
            Contributed::Biweekly => dec!(26),
        }
    }
}

// ==================================== Msg ==
pub enum Msg {
    Calculate,
    Reset,
    UpdateStartingValue(String),
    UpdateAdditionalMoneyMonthly(String),
    UpdateInterest(String),
    UpdateInflation(String),
    UpdateTimeInYears(String),
    UpdateCompounded(Compounded),
    UpdateContributed(Contributed),
}

// ==================================== Struct ==
#[allow(dead_code)]
#[derive(Debug, Serialize, Deserialize)]
pub struct CompoundingInterest {
    principal: Decimal,
    contributions: Decimal,
    interest: Decimal,
    inflation: Decimal,
    years: Decimal,
    final_result: Decimal,
    compounded: Compounded,
    contributed: Contributed,
}

fn default_header() -> AttrValue {
    AttrValue::from("Compound Interest Calculator")
}

// ==================================== Props ==
#[derive(Clone, PartialEq, Properties)]
pub struct Props {
    #[prop_or(default_header())]
    pub header_text: AttrValue,
    pub compound_interest_key: &'static str,
}

// ==================================== Component ==
impl Component for CompoundingInterest {
    type Message = Msg;
    type Properties = Props;

    fn create(ctx: &Context<Self>) -> Self {
        let stored_data =
            LocalStorage::get(&ctx.props().compound_interest_key).unwrap_or_else(|_| {
                CompoundingInterest {
                    principal: dec!(0),
                    contributions: dec!(0),
                    interest: dec!(0),
                    inflation: dec!(0),
                    years: dec!(0),
                    final_result: dec!(0),
                    compounded: Compounded::Monthly,
                    contributed: Contributed::Monthly,
                }
            });

        CompoundingInterest {
            principal: stored_data.principal,
            contributions: stored_data.contributions,
            interest: stored_data.interest,
            inflation: stored_data.inflation,
            years: stored_data.years,
            final_result: stored_data.final_result,
            compounded: stored_data.compounded,
            contributed: stored_data.contributed,
        }
    }

    // Return `true` if you need to re-render, `false` if not
    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Msg::Calculate => {
                self.calculate();
            }
            Msg::Reset => {
                self.reset();
            }
            Msg::UpdateStartingValue(input) => {
                let input = input.parse::<Decimal>().unwrap_or(dec!(0));
                self.principal = input;
                self.calculate();
            }
            Msg::UpdateAdditionalMoneyMonthly(input) => {
                let input = input.parse::<Decimal>().unwrap_or(dec!(0));
                self.contributions = input;
                self.calculate();
            }
            Msg::UpdateInterest(input) => {
                let input = input.parse::<Decimal>().unwrap_or(dec!(0));
                self.interest = input;
                self.calculate();
            }
            Msg::UpdateInflation(input) => {
                let input = input.parse::<Decimal>().unwrap_or(dec!(0));
                self.inflation = input;
                self.calculate();
            }
            Msg::UpdateTimeInYears(input) => {
                let input = input.parse::<Decimal>().unwrap_or(dec!(0));
                self.years = input;
                self.calculate();
            }
            Msg::UpdateCompounded(input) => {
                self.compounded = input;
                self.calculate();
            }
            Msg::UpdateContributed(input) => {
                self.contributed = input;
                self.calculate();
            }
        }

        LocalStorage::set(&ctx.props().compound_interest_key, &self)
            .expect("Failed to store data in LocalStorage");
        true
    }

    fn rendered(&mut self, _ctx: &Context<Self>, _first_render: bool) {
        log::info!("We've rendered!");
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let link = ctx.link(); // Allows us to set the scope
        let header_text = &ctx.props().header_text;

        let calculate = link.callback(|_| Msg::Calculate);
        let reset = link.callback(|_| Msg::Reset);

        let stored_data =
            LocalStorage::get(&ctx.props().compound_interest_key).unwrap_or_else(|_| {
                CompoundingInterest {
                    principal: dec!(0),
                    contributions: dec!(0),
                    interest: dec!(0),
                    inflation: dec!(0),
                    years: dec!(0),
                    final_result: dec!(0),
                    compounded: Compounded::Monthly,
                    contributed: Contributed::Monthly,
                }
            });

        html! {
            <>
            <div>
                <p class={ "center" }>{ header_text }</p>
                { self.starting_value_html(ctx, &stored_data) }
                { self.contribution_html(ctx, &stored_data) }
                { self.contributed_html(ctx, &stored_data) }
                { self.interest_html(ctx, &stored_data) }
                { self.compounded_html(ctx, &stored_data) }
                { self.inflation_html(ctx, &stored_data) }
                { self.years_html(ctx, &stored_data) }
                { self.results_html() }
                <div class="center-center">
                    <button class="action-button" onclick={ calculate }>{ "Re-Calculate" }</button>
                    <button class="action-button grey" onclick={ reset }>{ "reset" }</button>
                </div>
            </div>
            </>
        }
    }
}

// ==================================== Business Logic Impl ==

impl CompoundingInterest {
    pub fn calculate(&mut self) {
        self.final_result = calculate_compounding_interest(
            self.principal,
            self.contributions,
            self.interest,
            self.inflation,
            self.years,
            self.compounded,
            self.contributed,
        );
    }

    pub fn reset(&mut self) {
        self.principal = dec!(0);
        self.contributions = dec!(0);
        self.interest = dec!(0);
        self.inflation = dec!(0);
        self.years = dec!(0);
        self.final_result = dec!(0);
        self.contributed = Contributed::Monthly;
        self.compounded = Compounded::Monthly;
    }
}

// ==================================== Html helpers ==

impl CompoundingInterest {
    fn results_html(&self) -> Html {
        html! {
            <>
            <div class="result-section">
                if DEBUG {
                    <p class="center">{ "DEBUG: Raw results" }</p>
                    <div class="center-center">
                        { format!("DEBUG: Raw Decimal final_result_caluculated_monthly: {}", self.final_result) }
                    </div>
                }
                <div class="result-item">
                    <span class="center">{ "Contributions" }</span>
                    <div class="center">
                        { fmt_string_as_currency(
                            (self.principal + (self.contributions * self.years * self.contributed.value_decimal())).to_string())
                        }
                    </div>
                </div>
                <div class="result-item">
                    <span class="center">{ "Interest" }</span>
                    <div class="center green">
                        { fmt_string_as_currency(
                            (self.final_result - self.principal - (self.contributions * self.years * self.contributed.value_decimal())).to_string())
                        }
                    </div>
                </div>
                <div class="result-item">
                    <span class="center">{ "Final Value" }</span>
                    <div class="center-center">
                        { fmt_string_as_currency(self.final_result.to_string()) }
                    </div>
                </div>
            </div>
            </>
        }
    }

    fn starting_value_html(&self, ctx: &Context<Self>, stored_data: &CompoundingInterest) -> Html {
        let update_starting_value = ctx.link().batch_callback(|event: Event| {
            let input: HtmlInputElement = event.target_unchecked_into();
            let value = input.value();
            Some(Msg::UpdateStartingValue(value))
        });

        html! {
            <>
            <div class={ "input_block" }>
                <label class={ "input_label" }>
                    { "Starting value" }
                </label>
                <input class={ "input_input" }
                    value={ AttrValue::from(stored_data.principal.to_string())}
                    onchange={ update_starting_value }
                    id="starting_value"
                />
            </div>
            </>
        }
    }

    fn contribution_html(&self, ctx: &Context<Self>, stored_data: &CompoundingInterest) -> Html {
        let update_monthly_contribution = ctx.link().batch_callback(|event: Event| {
            let input: HtmlInputElement = event.target_unchecked_into();
            let value = input.value();
            Some(Msg::UpdateAdditionalMoneyMonthly(value))
        });

        html! {
            <>
            <div class={ "input_block" }>
                <label class={ "input_label" }>
                    { "Contribution" }
                </label>
                <input class={ "input_input" }
                    value={ AttrValue::from(stored_data.contributions.to_string())}
                    onchange={ update_monthly_contribution }
                />
            </div>
            </>
        }
    }

    fn contributed_html(&self, ctx: &Context<Self>, _stored_data: &CompoundingInterest) -> Html {
        let set_annually = ctx
            .link()
            .batch_callback(|_| Some(Msg::UpdateContributed(Contributed::Annually)));

        let set_quarterly = ctx
            .link()
            .batch_callback(|_| Some(Msg::UpdateContributed(Contributed::Quarterly)));

        let set_monthly = ctx
            .link()
            .batch_callback(|_| Some(Msg::UpdateContributed(Contributed::Monthly)));

        let set_biweekly = ctx
            .link()
            .batch_callback(|_| Some(Msg::UpdateContributed(Contributed::Biweekly)));

        html! {
            <>
            <div class={ "input_block" }>
                <div><label class={ "input_label" }>{ "Contributed" }</label></div>
                <button class={ if self.contributed == Contributed::Annually {"item-chosen"} else {"item-not-chosen"} } onclick={ set_annually }>
                    { "Annually" }
                </button>
                <button class={ if self.contributed == Contributed::Quarterly {"item-chosen"} else {"item-not-chosen"} } onclick={ set_quarterly }>
                    { "Quarterly" }
                </button>
                <button class={ if self.contributed == Contributed::Monthly {"item-chosen"} else {"item-not-chosen"} } onclick={ set_monthly }>
                    { "Monthly" }
                </button>
                <button class={ if self.contributed == Contributed::Biweekly {"item-chosen"} else {"item-not-chosen"} } onclick={ set_biweekly }>
                    { "bi-weekly" }
                </button>
            </div>
            </>
        }
    }

    fn compounded_html(&self, ctx: &Context<Self>, _stored_data: &CompoundingInterest) -> Html {
        let set_annually = ctx
            .link()
            .batch_callback(|_| Some(Msg::UpdateCompounded(Compounded::Annually)));

        let set_monthly = ctx
            .link()
            .batch_callback(|_| Some(Msg::UpdateCompounded(Compounded::Monthly)));

        let set_daily = ctx
            .link()
            .batch_callback(|_| Some(Msg::UpdateCompounded(Compounded::Daily)));

        html! {
            <>
            <div class={ "input_block" }>
                <div><label class={ "input_label" }>{ "Compounded" }</label></div>
                <button class={ if self.compounded == Compounded::Annually {"item-chosen"} else {"item-not-chosen"} } onclick={ set_annually }>
                    { "Annually" }
                </button>
                <button class={ if self.compounded == Compounded::Monthly {"item-chosen"} else {"item-not-chosen"} } onclick={ set_monthly }>
                    { "Monthly" }
                </button>
                <button class={ if self.compounded == Compounded::Daily {"item-chosen"} else {"item-not-chosen"} } onclick={ set_daily }>
                    { "Daily" }
                </button>
            </div>
            </>
        }
    }

    fn interest_html(&self, ctx: &Context<Self>, stored_data: &CompoundingInterest) -> Html {
        let update_interest = ctx.link().batch_callback(|event: Event| {
            let input: HtmlInputElement = event.target_unchecked_into();
            let value = input.value();
            Some(Msg::UpdateInterest(value))
        });

        html! {
            <>
            <div class={ "input_block" }>
                <label class={ "input_label" }>
                    { "Interest" }
                </label>
                <input class={ "input_input" }
                    value={ AttrValue::from(stored_data.interest.to_string())}
                    onchange={ update_interest }
                />
            </div>
            </>
        }
    }

    fn inflation_html(&self, ctx: &Context<Self>, stored_data: &CompoundingInterest) -> Html {
        let update_inflation = ctx.link().batch_callback(|event: Event| {
            let input: HtmlInputElement = event.target_unchecked_into();
            let value = input.value();
            Some(Msg::UpdateInflation(value))
        });

        html! {
            <>
            <div class={ "input_block" }>
                <label class={ "input_label" }>
                    { "Inflation" }
                </label>
                <input class={ "input_input" }
                    value={ AttrValue::from(stored_data.inflation.to_string())}
                    onchange={ update_inflation }
                />
            </div>
            </>
        }
    }

    fn years_html(&self, ctx: &Context<Self>, stored_data: &CompoundingInterest) -> Html {
        let update_years = ctx.link().batch_callback(|event: Event| {
            let input: HtmlInputElement = event.target_unchecked_into();
            let value = input.value();
            Some(Msg::UpdateTimeInYears(value))
        });

        html! {
            <>
            <div class={ "input_block" }>
                <label class={ "input_label" }>
                    { "Years" }
                </label>
                <input class={ "input_input" }
                    value={ AttrValue::from(stored_data.years.to_string())}
                    onchange={ update_years }
                />
            </div>
            </>
        }
    }
}

// #[cfg(test)]
// mod tests {
// use super::*;
// }
