use yew::{html, Component};

// use crate::just_text;
// use crate::brianbthing;
use crate::cars;
use crate::test_app;
use crate::dynamic_calculator::dynamic_calculator_ui;

pub struct Base {}

impl Component for Base {
    type Message = ();
    type Properties = ();

    fn create(_ctx: &yew::Context<Self>) -> Self {
        Self {}
    }

    fn view(&self, _ctx: &yew::Context<Self>) -> yew::Html {
        html! {
            <div class="container">
                <>
                    <dynamic_calculator_ui::DynamicAppController />
                </>
                <div class="item" hidden=true>
                    <cars::Cars />
                </div>

                <div class="item" hidden=true>
                    <test_app::Counter />
                </div>
            </div>
        }
    }
}
