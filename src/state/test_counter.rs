use serde::{Deserialize, Serialize};
use yewdux::prelude::Store;

#[derive(Debug, Default, Clone, PartialEq, Eq, Deserialize, Serialize, Store)]
#[store(storage = "local")]
pub struct TestCounter {
    pub counter: i64,
}
